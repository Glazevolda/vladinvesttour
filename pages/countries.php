<!--////////////////////////////////////
    //-----------------Countries(Fourth-Page)
    ///////////////////////////////////-->

<section class="Countries">

    <h2>Направления</h2>


    <div>
        <h3>Европа</h3>

        <div>
            <a href="" href="">
                <div class="at-icon"></div>
                <span>Австрия</span>
            </a>
        </div>
        <div>
            <a href="" href="">
                <div class="ad-icon"></div>
                <span>Андорра</span>
            </a>
        </div>
        <div>
            <a href="" href="">
                <div class="be-icon"></div>
                <span>Бельгия</span>
            </a>
        </div>
        <div>
            <a href="" href="">
                <div class="gb-icon"></div>
                <span>Великобритания</span>
            </a>
        </div>
        <div>
            <a href="" href="">
                <div class="de-icon"></div>
                <span>Германия</span>
            </a>
        </div>

        <div>
            <a href="">
                <div class="gr-icon"></div>
                <span>Греция</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="ie-icon"></div>
                <span>Ирландия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="es-icon"></div>
                <span>Испания</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="it-icon"></div>
                <span>Италия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="cy-icon"></div>
                <span>Кипр</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="lu-icon"></div>
                <span>Люксембург</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="nl-icon"></div>
                <span>Нидерланды</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="pt-icon"></div>
                <span>Португалия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="tr-icon"></div>
                <span>Турция</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="ua-icon"></div>
                <span>Украина</span>
            </a>
        </div>
    </div>


    <div>
        <h3>Азия</h3>

        <div>
            <a href="">
                <div class="ge-icon"></div>
                <span>Грузия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="il-icon"></div>
                <span>Израиль</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="in-icon"></div>
                <span>Индия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="id-icon"></div>
                <span>Индонезия</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="cn-icon"></div>
                <span>Китай</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="mv-icon"></div>
                <span>Мальдивы</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="ae-icon"></div>
                <span>ОАЭ</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="sg-icon"></div>
                <span>Сингапур</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="th-icon"></div>
                <span>Таиланд</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="uz-icon"></div>
                <span>Узбекистан</span>
            </a>
        </div>

        <h3>Южная Америка</h3>

        <div>
            <a href="">
                <div class="br-icon"></div>
                <span>Бразилия</span>
            </a>
        </div>


    </div>
    <div>
        <h3>Северная Америка</h3>

        <div>
            <a href="">
                <div class="do-icon"></div>
                <span>Доминиканская республика</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="cu-icon"></div>
                <span>Куба</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="mx-icon"></div>
                <span>Мексика</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="um-icon"></div>
                <span>США</span>
            </a>
        </div>


        <h3>Африка</h3>

        <div>
            <a href="">
                <div class="ye-icon"></div>
                <span>Египет</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="ke-icon"></div>
                <span>Кения</span>
            </a>
        </div>

        <div>
            <a href="">
                <div class="mu-icon"></div>
                <span>Маврикий</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="sc-icon"></div>
                <span>Сейшельские острова</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="tz-icon"></div>
                <span>Танзания</span>
            </a>
        </div>
        <div>
            <a href="">
                <div class="za-icon"></div>
                <span>ЮАР</span>
            </a>
        </div>

    </div>


</section>