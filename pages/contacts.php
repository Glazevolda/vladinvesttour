<!--////////////////////////////////////
//-----------------Contacts(Fifth-Page)
///////////////////////////////////-->
<div class="Contacts-Wrap">
    <!-------------------Contacts-Left-->

    <div class="Contacts-Left">
        <section class="Contacts-Country">
            <h2>Москва, Россия</h2>
            <dl>
                <dt>Адрес:</dt>
                <dd>Проспект Мира, дом 187, офис 128.</dd>
            </dl>
            <dl>
                <dt>Тел.:</dt>
                <dd>
                    <span>+7 (495) 925-66-99</span>
                    <span>+7 (495) 937-35-35</span>

                </dd>
            </dl>
            <dl>
                <dt>E-mail</dt>
                <dd>vladinvest.info@mail.ru</dd>
            </dl>
            <dl>
                <dt>Время работы:</dt>
                <dd>Пн-Пт с 10:00 до 19:00,Сб с 11:00 до 17:00</dd>
            </dl>
        </section>
        <section class="Contacts-Country">
            <h2>Киев, Украина</h2>
            <dl>
                <dt>Адрес:</dt>
                <dd>Проспект Мира, дом 187, офис 128.</dd>
            </dl>
            <dl>
                <dt>Тел.:</dt>
                <dd>
                    <span>+7 (495) 925-66-99</span>
                    <span>+7 (495) 937-35-35</span>

                </dd>
            </dl>
            <dl>
                <dt>E-mail</dt>
                <dd>vladinvest.info@mail.ru</dd>
            </dl>
            <dl>
                <dt>Время работы:</dt>
                <dd>Пн-Пт с 10:00 до 19:00,Сб с 11:00 до 17:00</dd>
            </dl>
        </section>
        <section class="Contacts-Country">
            <h2>Минск, Белоруссия</h2>
            <dl>
                <dt>Адрес:</dt>
                <dd>Проспект мира, дом 187, офис 128.</dd>
            </dl>
            <dl>
                <dt>Тел.:</dt>
                <dd>
                    <span>+7 (495) 925-66-99</span>
                    <span>+7 (495) 937-35-35</span>

                </dd>
            </dl>
            <dl>
                <dt>E-mail</dt>
                <dd>vladinvest.info@mail.ru</dd>
            </dl>
            <dl>
                <dt>Время работы:</dt>
                <dd>Пн-Пт с 10:00 до 19:00,Сб с 11:00 до 17:00</dd>
            </dl>
        </section>
    </div>
    <!-------------------Contacts-Right-->

    <div class="Contacts-Right">
        <div class="Contacts-Map">
            <script type="text/javascript" charset="utf-8"
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=15MWCu72gOQMVmRXpYT_WssVMvDcwR0H&width=100%&height=350&lang=ru_UA&sourceType=constructor"></script>
        </div>
        <div class="Contacts-Form">
            <h2>Напишите нам</h2>

            <div class="Form-Info">
                <input type="text" placeholder="Имя"/>
                <input type="text" placeholder="Телефон"/>
                <input type="text" placeholder="E-mail"/>
            </div>
            <div class="Form-Send">
                <textarea name="" placeholder="Ваше сообщение"></textarea>
                <button class="Form-Button">Отправить</button>
            </div>
        </div>
    </div>
</div>
