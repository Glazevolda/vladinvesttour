<!--//////////////////////////////////
    //-----------------Directions(Second-Page)
    ///////////////////////////////////-->

<div class="Directions-Wrap">
    <section class="Directions">
        <h2 class="Directions-Title"><span>Направления</span><span class="Directions-Country">Великобритания</span></h2>

        <?php
        for ($Index = 0; $Index < 6; $Index++) {
            require './modules/direction_module.php';
        }
        ?>

    </section>
</div>
