<!--////////////////////////////////////
    //-----------------Tours(First Page)
    ///////////////////////////////////-->


    <div class="Tours-Wrap">
        <div class="Tours">
            <!--///------------Tours-Check-->
            <div class="Tours-Check">

                <div class="Tour-Window">
                    <div class="Tour-Previous">
                        <svg>
                            <use xlink:href="/img/ico/d-sprite.svg#d-arrow"></use>
                        </svg>
                    </div>
                    <div class="Tour-Next">
                        <svg>
                            <use xlink:href="/img/ico/d-sprite.svg#d-arrow"></use>
                        </svg>
                    </div>
                    <ul class="Tour-Nav">
                        <li>
                            <input id="#Tour-1" type="radio"/>
                            <label for="#Tour-1">
                                <div></div>
                            </label>
                        </li>
                        <li>
                            <input id="#Tour-2" type="radio"/>
                            <label for="#Tour-2">
                                <div></div>
                            </label>
                        </li>
                        <li>
                            <input id="#Tour-3" type="radio"/>
                            <label for="#Tour-3">
                                <div></div>
                            </label>
                        </li>
                        <li>
                            <input id="#Tour-4" type="radio"/>
                            <label for="#Tour-4">
                                <div></div>
                            </label>
                        </li>
                        <li>
                            <input id="#Tour-5" type="radio"/>
                            <label for="#Tour-5">
                                <div></div>
                            </label>
                        </li>

                    </ul>

                </div>
                <div class="Tour-Bottom">
                    <h1>Яркий зимний отдых в горах</h1>
                    <button Tour-Button>Посмотреть доступные зимние туры</button>
                </div>
            </div>
            <!--///Tours-News-->
        </div>
        <?php
        require_once './modules/sidebar_section.php';
        ?>
        <section class="Tours-News">
            <h2>Новости</h2>

            <div>
                <img src="/img/pic/beach.jpg" alt=""/>
                <article class="Tours-New">
                    <h3>Фестиваль Comedy: тематические вечеринки.
                        Когда веселье день за днём!</h3>

                    <span class="Tours-Stock">Акция!</span>
                    <span class="Tours-Date">до 12 января 2016</span>

                    <p>Танцевать и веселиться на вечеринках — это классно!
                        Но намного интереснее, если вечеринка тематическая,
                        а гости разодеты в креативные костюмы! Предлагаем вашему
                        вниманию перечень тематических вечеринок Фестиваля Comedy в Сочи!
                    </p>
                </article>
            </div>
            <div>
                <img src="/img/pic/beach.jpg" alt=""/>

                <article class="Tours-New">
                    <h3>Фестиваль Comedy: тематические вечеринки.
                        Когда веселье день за днём!</h3>

                    <span class="Tours-Date">12 января 2016</span>

                    <p>Танцевать и веселиться на вечеринках — это классно!
                        Но намного интереснее, если вечеринка тематическая,
                        а гости разодеты в креативные костюмы! Предлагаем вашему
                        вниманию перечень тематических вечеринок Фестиваля Comedy в Сочи!
                    </p>
                </article>

            </div>
            <div>
                <img src="/img/pic/beach.jpg" alt=""/>
                <article class="Tours-New">
                    <h3>Фестиваль Comedy: тематические вечеринки.
                        Когда веселье день за днём!</h3>

                    <span class="Tours-Date">12 января 2016</span>

                    <p>Танцевать и веселиться на вечеринках — это классно!
                        Но намного интереснее, если вечеринка тематическая,
                        а гости разодеты в креативные костюмы! Предлагаем вашему
                        вниманию перечень тематических вечеринок Фестиваля Comedy в Сочи!
                    </p>
                </article>

            </div>
        </section>

        <!--//-----------------Tours-->


    </div>







