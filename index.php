<!doctype html>
<html lang="rus">
<head>
    <meta charset="UTF-8">
    <title>Vladinvesttour</title>
    <link rel="stylesheet" href="/css/index.min.css"/>
</head>
<body>

<?php
require_once './modules/header.php';
?>

<main>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require_once './pages/tours.php';
    } else {
        require_once './pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>
</main>

<?php
require_once './modules/footer.php';
?>
<?php
require_once './modules/modal_form.php';
?>
</body>
</html>