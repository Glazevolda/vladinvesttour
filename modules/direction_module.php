<!--    ///-------------Direction-->
<div class="Direction">
    <div class="Direction-Top">
        <img src="/img/pic/Greath-Britain.jpg" alt=""/>
        <span class="Direction-Price">12 000 руб.</span>
    </div>
    <section>

        <h2>Замки и поместья Великобритании</h2>
        <p>
            Экскурсионный тур: Лондон, Обзорная экскурсия,
            музей Мадам Тюссо, Оксфорд,
            Хемптон Корт, Виндзорский замок.
        </p>
        <dl>
            <dt>Дней от:</dt>
            <dd>12</dd>
        </dl>
        <button class="Direction-Button">Подробнее</button>


    </section>

</div>