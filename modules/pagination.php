<!--//////////////////////////////////////////////
   /////////////////Pagination///////////////////
  //////////////////////////////////////////////-->
<div class="Paginations">
    <div class="Pagination-Top">
        <span>1</span>
        <span>2</span>
        <span>3</span>
        <span>4</span>
        <span>5</span>
    </div>
    <div class="Pagination">
        <div class="Pagination-Start">
            <svg></svg>
            <svg></svg>
        </div>
        <div class="Pagination-Slide">
            <span class="Pagination-Slider"></span>
        </div>
        <div class="Pagination-End">
            <svg></svg>
            <svg></svg>
        </div>

    </div>
</div>
<div class="Paginations-2">
    <svg class="Pagination-Previous"></svg>
    <span>1</span>
    <span>2</span>
    <span>3</span>
    <span>...</span>
    <span>9</span>
    <span>10</span>
    <span>11</span>
    <svg class="Pagination-Next"></svg>
</div>