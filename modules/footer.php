<!--////////////////////////////////////
//-----------------FOOTER
///////////////////////////////////-->
<footer>
    <div class="Footer-Menu">
        <div class="Footer-Nav">
            <a>Главная страница</a>
            <a>Туры и цены</a>
            <a>Направления</a>
            <a>Корпоративным клиентам</a>
            <a>Контакты</a>
        </div>
        <div class="Footer-Socials">
            <span>Мы в социальных сетях</span>
            <a href="">
                <svg class="Socials-Twitter">
                    <use xlink:href="/img/ico/d-sprite-socials.svg#d-twitter"></use>
                </svg>
            </a>
            <a href="">
                <svg class="Socials-Vk">
                    <use xlink:href="/img/ico/d-sprite-socials.svg#d-vk"></use>
                </svg>

            </a>
            <a href="">

                <svg class="Socials-Facebook">
                    <use xlink:href="/img/ico/d-sprite-socials.svg#d-facebook"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="Copyright-Wrap">
        <div class="Copyright">
            <span>© 2015 Туристическое агенство Владинвесттур</span>

            <div class="Copyright-Developers">
                <span>Разработано в</span>
                <a href="">Progress Time</a>
            </div>
        </div>
    </div>

</footer>