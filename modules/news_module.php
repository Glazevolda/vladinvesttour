<!--Aside-Menu-->
<aside class="News-Module">

    <!--///------------Sidebar-News-->
    <article class="Sidebar-News">
        <div class="Sidebar-Stock">
            <span>Акция!</span>
        </div>
        <h2>Незабываемое путешествие в Америку!</h2>

        <p class="Stock-Alert">Акция действительна до 24 Января 2016</p>

        <p>Незабываемое путешествие в Америку со скидкой -5% на авиабилеты в Нью-Йорк,
            Чикаго, Майами, Лос-Анджелес, Вашингтон, Сан-Франциско, Лас-Вегас!
        </p>
        <a class="Sidebar-Button" href="">Подробнее</a>

    </article>
    <article class="Sidebar-News">
        <div class="Sidebar-Stock">
            <span>Акция!</span>
        </div>
        <h2>KLM запускает новые сезонные направления</h2>

        <p>Авиакомпания KLM в летний период 2016
            года предлагает семь новых сезонных направлений:
            Генуя, Валенсия, Саутгемтон, Инвернесс, Дрезден, Солт-Лейк-Сити, Ибица...
        </p>
        <a class="Sidebar-Button" href="">Подробнее</a>

    </article>
    <article class="Sidebar-News">
        <div class="Sidebar-Stock">
            <span>Акция!</span>
        </div>
        <h2>KLM запускает новые сезонные направления</h2>

        <p>Авиакомпания KLM в летний период 2016
            года предлагает семь новых сезонных направлений:
            Генуя, Валенсия, Саутгемтон, Инвернесс, Дрезден, Солт-Лейк-Сити, Ибица...
        </p>
        <a class="Sidebar-Button" href="">Подробнее</a>

    </article>

</aside>