<!--////////////////////////////////////
//-----------------HEADER
///////////////////////////////////-->

<header>
    <div class="Header-Top">
        <a href="/" class="Header-Logo"></a>
        <div class="Header-Search">
            <svg class="Header-Looking">
                <use xlink:href="/img/ico/d-sprite.svg#d-search"></use>
            </svg>
            <input type="text" placeholder="поиск по сайту ..."/>
            <button>Искать</button>
        </div>
        <ul class="Header-Exchange">
            <li>Внутренний курс валют:</li>
            <li>
                <svg class="Header-Dollar">
                    <use xlink:href="/img/ico/d-sprite.svg#d-dollar"></use>
                </svg>
                <span class="Header-Currency">USD:</span>
                <span class="Header-Rate">75.25</span>
            </li>
            <li>
                <svg class="Header-Euro">
                    <use xlink:href="/img/ico/d-sprite.svg#d-euro"></use>
                </svg>
                <span class="Header-Currency">EUR:</span>
                <span class="Header-Rate">82.50</span>
            </li>
        </ul>
        <ul class="Header-Contacts">
            <li>Наши телефоны:</li>
            <li>+7 (495) 925-66-99</li>
            <li>+7 (495) 937-35-35</li>
        </ul>

    </div>
    <div class="Header-Menu">
        <nav>
            <div class="Header-Shadow"></div>
            <a>Главная страница</a>
            <a>Туры и цены</a>
            <a>Направления</a>
            <a>Корпоративным клиентам</a>
            <a>Контакты</a>
        </nav>

    </div>
</header>