<!--//---------------Sidebar-Selection-->
<aside class="Sidebar-Selection">
    <section class="Selection-Tour">
        <h2>Быстрый подбор тура</h2>
        <span>Откуда Вы планируете вылетать?</span>

        <div class="Selection-Select">
            <select name="" id="Select-Town"></select>
            <span>Выберите город</span>
            <svg class="Selection-Arrow">
                <use xlink:href="/img/ico/d-sprite.svg#d-arrow"></use>
            </svg>
        </div>
        <span>Где Вы хотите отдохнуть?</span>

        <div class="Selection-Select">
            <select name="" id="Select-Country"></select>
            <span>Выберите страну</span>
            <svg class="Selection-Arrow">
                <use xlink:href="/img/ico/d-sprite.svg#d-arrow"></use>
            </svg>
        </div>
        <span>Дата вылета</span>

        <div class="Selection-Select">
            <select name="" id="Select-Date"></select>
            <span>Выберите дату</span>
            <svg class="Selection-Date">
                <use xlink:href="/img/ico/d-sprite.svg#d-calendar"></use>
            </svg>
        </div>
        <span>Длительность</span>

        <div class="Selection-Select">
            <select name="" id="Select-Duration"></select>
            <span>Количество дней</span>
            <svg class="Selection-Arrow">
                <use xlink:href="/img/ico/d-sprite.svg#d-arrow"></use>
            </svg>
        </div>
        <div class="Selection-Bottom">
            <button class="Selection-Button">Подобрать тур</button>
            <span class="Selection-Search"><a href="/">Расширенный поиск туров</a></span>

        </div>


    </section>
    <section class="Sidebar-Infos">
        <h2>Полезная информация</h2>

        <div>
            <a href="">
                <img class="Sidebar-Visa" src="/img/ico/d-visa.png" alt=""/>
                <span>Оформление визы</span>
            </a>

        </div>
        <div>
            <a href="">
                <img class="Sidebar-People" src="/img/ico/d-people.png" alt=""/>
                <span>Страховка</span>
            </a>
        </div>
        <div>
            <a href="">
                <img class="Sidebar-Car" src="/img/ico/d-car.png" alt=""/>
                <span>Аренда автомобилей</span>
            </a>

        </div>
    </section>
</aside>